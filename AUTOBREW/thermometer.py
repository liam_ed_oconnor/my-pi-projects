import math
import time
import Adafruit_CharLCD as LCD
import os
import RPi.GPIO as GPIO
import glob
import time
os.system('modprobe w1-gpio') 
os.system('modprobe w1-therm')
GPIO.setmode(GPIO.BOARD)
relay=16

GPIO.setup(relay, GPIO.OUT)
base_dir = '/sys/bus/w1/devices/' 
device_folder = glob.glob(base_dir + '28*')[0] 
device_file = device_folder + '/w1_slave' 
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines 
def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.1)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
	temp_c =round(temp_c,1) 
      #  temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c

lcd = LCD.Adafruit_CharLCDPlate()	
lcd.set_color(1.0,1.0,1.0)
lcd.message('Choose Set Point')
counter=20.0
x=False
while x==False:
	time.sleep(0.1)
	if lcd.is_pressed(LCD.UP)==True:
	        counter=counter+0.1
        elif lcd.is_pressed(LCD.DOWN)==True:
                counter=counter-0.1
	
	
	lcd.set_cursor(0,1)
# 		lcd.message('\n\n')
#		lcd.set_cursor(0,0)
	lcd.message(str(counter))
	
	if lcd.is_pressed(LCD.SELECT)==True:
		x=True


	


sp=counter
lcd.clear()
lcd.message('TEMP      ')
lcd.set_cursor(0,1)
lcd.message('SET POINT '+ str(sp)+ 'C')
while True:
	temp=str(read_temp())
	lcd.set_cursor(10,0)
	lcd.message(temp+ 'C')
	lcd.set_cursor(10,1)
	lcd.message(str(sp))

	if read_temp()<sp-1:
		GPIO.output(relay,1)
		lcd.set_color(0.0, 0.0, 1.0)
	#	lcd.message('\nTOO COLD   ')
		print "ON"
	elif read_temp()>sp+1:
		lcd.set_color(1.0,0.0,0.0)
	#	lcd.message('\nTOO HOT    ')
		GPIO.output(relay,0)
		print "OFF"
	else:
		lcd.set_color(0.0,1.0,0.0)
	#	lcd.message('\nJUST RIGHT   ')
#	time.sleep(0.1)
	
	if lcd.is_pressed(LCD.SELECT):
		
		print 'SELECT'
		lcd.set_color(1.0,1.0,1.0)
		lcd.set_cursor(10,1)
		lcd.blink(1)
		change=1
		time.sleep(2)
		while change==1:
			time.sleep(0.1)
			if lcd.is_pressed(LCD.UP)==True:
        	        	sp=sp+0.1
	                        lcd.set_cursor(10,1)
        	                lcd.message(str(sp))
				lcd.set_cursor(10,1)	
				print sp
        		elif lcd.is_pressed(LCD.DOWN)==True:
                		sp=sp-0.1
				print sp
                	        lcd.set_cursor(10,1)
                        	lcd.message(str(sp))
				lcd.set_cursor(10,1)
		
			if lcd.is_pressed(LCD.SELECT):
				lcd.blink(False)
				print 'change off'
				change=0


	print(read_temp())
#	lcd.set_cursor(0,0)
#	lcd.message('\n\n')
#	lcd.set_cursor(0,0)	

